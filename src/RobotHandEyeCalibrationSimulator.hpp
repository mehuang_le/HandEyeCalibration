#ifndef _HANDEYECALIBATION_RWSPLUGIN_HPP_
#define _HANDEYECALIBATION_RWSPLUGIN_HPP_

#include <iostream>
#include <stdio.h>

//RobWorkStudio
#include <rws/RobWorkStudioPlugin.hpp>
#include <RobWorkStudio.hpp>

//RobWork
#include <rw/rw.hpp> 
#include <rw/loaders/WorkCellLoader.hpp>
#include <rwlibs/simulation/GLFrameGrabber.hpp>
//FIXME: can not use rw.hpp header, xcerces error arise.
//FIXED by comment line 34 of rw.hpp,  #include "loaders.hpp"

//opencv
#include <opencv2/opencv.hpp>

//UI
#include "ui_plugin.h"

//QT
#include <QTimer>
#include <QPushButton>

//threads
#include <boost/thread.hpp>

//namespace

class RWSPlugin: public rws::RobWorkStudioPlugin, private Ui::DockWidget
{
Q_OBJECT
Q_INTERFACES( rws::RobWorkStudioPlugin )
#if RWS_USE_QT5
Q_PLUGIN_METADATA(IID "dk.sdu.mip.Robwork.RobWorkStudioPlugin/0.1" FILE "plugin.json")
#endif
public:

    RWSPlugin();
	
	virtual ~RWSPlugin();

	virtual void initialize();

	virtual void open(rw::models::WorkCell* workcell);

	virtual void close();

	/**
	 * @brief Listen for change in workcell.
	 *
	 * This method can be safely called from non-qt threads.
	 *
	 * @param notUsed [in] not used.
	 */
	void workcellChangedListener(int notUsed);

	/**
	 * @brief Listen for generic events:
	 * - WorkcellUpdated event makes plugin refresh to represent new workcell configuration (i.e. new frames & devices).
	 */
	void genericEventListener(const std::string& event);

private slots:
    
	void btnClicked();

	void stateChangedListener(const rw::kinematics::State& state);

	void updateScene();

protected:

	QTimer *_timer1;

	rw::models::WorkCell* _workCell;
	rw::kinematics::State _state;
	rw::models::Device::Ptr _deviceRobot;
	rw::math::Q _robotConfig;

	rw::kinematics::Frame* _cameraFrame;

	rwlibs::simulation::GLFrameGrabber* _frameGrabber;

	cv::Mat _image;

	std::vector<rw::math::Transform3D<double> > _pose;

	int _imageNumber;

private:
	void setupCamera(void);

	bool _updating;
};

#endif
